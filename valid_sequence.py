def valid_seq(n, k, j):
    # Treat n=1 and n=2 as special cases
    if n==1:
        if j==1:      # if n=1, the sequence both begins and ends with 1
            return 1
        else:
            return 0  
    if n==2:
        if j==1:
            return 0
        else:
            return 1
    # If n > 2: Add one number at a time until we get to (n-1) numbers. Keep track of the number of current sequences ending with j and not ending with j.
    # At last, all the sequences of (n-1) numbers not ending with j can be made into a valid sequence by adding j at the end.
    end_j = 0
    not_end_j = 0
    for i in range(2,n):
        if i==2:
            if j==1:
                end_j = 0
                not_end_j = k-1
            else:
                end_j = 1
                not_end_j = k-2
        else:
            # Since the numbers can be very large, we need to use the modulus in every step, which doesn't change the final result.
            a, b = end_j%(10**10+7), not_end_j%(10**10+7)
            end_j = b
            not_end_j = a*(k-1)+b*(k-2)     
    return not_end_j%(10**10+7)

