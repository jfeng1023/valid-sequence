# Valid Sequence

This was a coding problem as part of the Data Incubator challenge in May 2019.

A sequence of n numbers is considered valid if the sequence begins with 1, ends with a given number j, and no two adjacent numbers are the same. Sequences may use any integers between 1 and a given number k, inclusive (also 1<=j<=k). Given parameters n, j, and k, count the number of valid sequences. The number of valid sequences may be very large, so express your answer modulo 10^10+7.